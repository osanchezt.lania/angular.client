import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ContainerComponent } from './components/container/container.component';
import { FooterComponent } from './components/footer/footer.component';

import { DataTablesModule } from 'angular-datatables';
import { RouterModule, Route } from '@angular/router';
import { AgregarComponent } from './components/agregar/agregar.component';
import { ModificarComponent } from './components/modificar/modificar.component';
import { EliminarComponent } from './components/eliminar/eliminar.component';
import { ReactiveFormsModule } from '@angular/forms'
import { from } from 'rxjs';
import { EditComponentComponent } from './components/edit-component/edit-component.component';
import { LoginComponent } from './components/login/login.component';

//Providers
import { DataService } from './services/data.service';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Route[] = [
{path: 'login', component: LoginComponent, canActivate: [LoginGuard]},
  {path: '', component: ContainerComponent, canActivate: [AuthGuard]},
  {path: 'agregar', component: AgregarComponent, canActivate: [AuthGuard]},
  {path: 'modificar', component: ModificarComponent, canActivate: [AuthGuard]},
  {path: 'eliminar', component: EliminarComponent, canActivate: [AuthGuard]},
  {path: 'modificar/desembolso/:id', component: EditComponentComponent, canActivate: [AuthGuard]},
  {path: '**', pathMatch: 'full', redirectTo: 'login'}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContainerComponent,
    FooterComponent,
    AgregarComponent,
    ModificarComponent,
    EliminarComponent,
    EditComponentComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    DataTablesModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)

  ],
  providers: [
    {provide: JWT_OPTIONS, useValue: JWT_OPTIONS},
    DataService,
    JwtHelperService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
