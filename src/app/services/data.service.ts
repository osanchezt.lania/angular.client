import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Lista } from '../Lista';
import { Sucursal } from '../models/Sucursal'
import { Region } from '../models/Region'
import { Desembolso } from '../models/Desembolso';
import { User } from '../models/User';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private loggedIn = new BehaviorSubject<boolean>(false);
  URI = 'http://localhost:4000/operaciones';
  //URI = 'https://app-operaciones.herokuapp.com/operaciones';

  constructor(private __httpClient: HttpClient, private jwtHelper: JwtHelperService) { 
    //console.log('SERVICE IS WORKING!');
  }

  login(user: User){
    return this.__httpClient.post(`${this.URI}/singin`, user);
  }

  getOperaciones(): Observable<Lista>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/jason',
      'Authorization': 'Bearer ' + localStorage.getItem('token'),
    });
    return this.__httpClient.get<Lista>(`${this.URI}`, {headers});
  }

  getOp() {
    return this.__httpClient.get(this.URI, {observe: 'response'})
  }

  getOperacionesById(id: number): Observable<Desembolso> {
    return this.__httpClient.get<Desembolso>(`${this.URI}/${id}`);
  }

  getDesembolsoByDateSucursal(sucursal: Sucursal): Observable<Sucursal> {
    return this.__httpClient.post<Sucursal>(`${this.URI}/sucursal`, sucursal);
  }

  getDesembolsoByDateRegion(region: Region): Observable<Region> {
    return this.__httpClient.post<Region>(`${this.URI}/regional`, region);
  }

  saveDesembolso(desembolso: Desembolso): Observable<Desembolso>{
    return this.__httpClient.post<Desembolso>(this.URI, desembolso);
  }

  updateDesembolso(id: number, desembolso: Desembolso): Observable<Desembolso>{
    return this.__httpClient.put<Desembolso>(this.URI + `/${id}`, desembolso);
  }

  deleteDesembolso(id: number): Observable<Desembolso> {
    return this.__httpClient.delete<Desembolso>(this.URI + `/${id}`);
  }

  isAuth(): boolean {
    const token = localStorage.getItem('token');
    if (this.jwtHelper.isTokenExpired(token) || !localStorage.getItem('token')) {
      return false;
    }
    this.loggedIn.next(true);
    return true;
  }

  isLogin(): boolean {
    const token = localStorage.getItem('token');
    if (!this.jwtHelper.isTokenExpired(token) || localStorage.getItem('token')) {
      return false;
    }
    return true;
  }

  isLogout() {
    this.loggedIn.next(false);
    //console.log(this.loggedIn);
    if (localStorage.getItem('token')) {
      localStorage.removeItem('token');
    } else {
      console.log('Token empty');
    }
  }

  get isLogged(): Observable<boolean> {
    return this.loggedIn.asObservable();
  }

}
