import { Component, OnInit, ViewChild } from '@angular/core';
import { Lista } from 'src/app/Lista';
import { Sucursal } from 'src/app/models/Sucursal';
import { Region } from 'src/app/models/Region';
import { Desembolso } from '../../models/Desembolso';
import { DataService } from '../../services/data.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-eliminar',
  templateUrl: './eliminar.component.html',
  styleUrls: ['./eliminar.component.css']
})
export class EliminarComponent implements OnInit {

  lista: Lista[];
  sucursal: Sucursal;
  region: Region;
  desembolso: Desembolso = {
    id: null,
    nombre_ac: '',
    clientes: null,
    monto: null,
    banco: '',
    fecha_desembolso: '',
    sucursal: '',
    regional: ''
  };
  selectSucursal = false;
  selectRegional = false;
  isDeletedAlert: boolean = false;
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.regionalForm = this.createFormGroup();
    this.sucursalForm = this.createFormGroupSucursal();
    this.getOperaciones();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      lengthMenu: [10, 25, 50, 200],
      language: {
        url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json'
      }
    };
  }

  dismmisAlert(){
    this.isDeletedAlert = false;
  }

  getOperaciones () {
    this.dataService.getOperaciones().subscribe((res: any) => {
      //console.log(data);
      this.lista = res;
      this.dtTrigger.next();
      console.log(this.lista);
    });
  }

  deleteDesembolso(id: number) {
    const alerta = confirm("¿Desea eliminar el registro con id " + id + '?');
    if (alerta) {
      this.isDeletedAlert = true;
      this.dataService.deleteDesembolso(id).subscribe(
        res => {
          console.log(res);
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
          });
          this.getOperaciones();
        },
        err => console.error(err)
      );
    } else {
      this.isDeletedAlert = false;
    }
  }

  formSucursal() {
    //alert('Seleccionaste sucursal.')
    this.isDeletedAlert = false;
    this.selectSucursal = true;
    this.selectRegional = false;
    this.sucursalForm.reset();
    this.regionalForm.reset();
    this.lista = [];
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  findByDateSucursal(): void {
    if(this.sucursalForm.invalid) {
      alert("Completa campos");
    } else {
      //this.selectSucursal = false;
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
      this.sucursal = this.sucursalForm.value;
      this.dataService.getDesembolsoByDateSucursal(this.sucursal).subscribe( (res: any) => {
        console.log(this.sucursal);
        this.lista = res;
        this.dtTrigger.next();
      });
    }
  }

  formRegional() {
    this.isDeletedAlert = false;
    this.selectRegional = true;
    this.selectSucursal = false;
    this.sucursalForm.reset();
    this.regionalForm.reset();
    this.lista = [];
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  findByDateRegional(): void {
    if(this.regionalForm.invalid) {
      alert("Completa campos");
    } else {
      //alert('Campos completados.')
      //this.selectRegional = false;
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
      this.region = this.regionalForm.value;
      this.dataService.getDesembolsoByDateRegion(this.region).subscribe((res: any) => {
        console.log(this.region);
        //console.log(res);
        this.lista = res;
        this.dtTrigger.next();
      });
    }
  }





  //validacion de formulario SUCURSAL
  createFormGroupSucursal(){
    return new FormGroup({
      fechaSuc_1: new FormControl('', Validators.required),
      fechaSuc_2: new FormControl('', Validators.required),
      sucursalInput: new FormControl('', Validators.required),
    });
  }
  sucursalForm: FormGroup;
  get fechaSuc_1() { return this.sucursalForm.get('fechaSuc_1'); }
  get fechaSuc_2() { return this.sucursalForm.get('fechaSuc_2'); }
  get sucursalInput() { return this.sucursalForm.get('sucursalInput'); }

  //validacion de formulario REGIONAL
  createFormGroup(){
    return new FormGroup({
      fecha_1: new FormControl('', Validators.required),
      fecha_2: new FormControl('', Validators.required),
      regional: new FormControl('', Validators.required),
    });
  }
  regionalForm: FormGroup;
  get fecha_1() { return this.regionalForm.get('fecha_1'); }
  get fecha_2() { return this.regionalForm.get('fecha_2'); }
  get regional() { return this.regionalForm.get('regional'); }
}
