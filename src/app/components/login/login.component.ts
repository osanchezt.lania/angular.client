import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../../models/User'
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;
  invalidUser = false;
  isLogin: boolean;

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.createFormGroup();
    this.dataService.isLogged.subscribe((res) => (this.isLogin = res));
    //console.log(this.isLogin);
  }

  createFormGroup() {
    return new FormGroup({
      username: new FormControl('', Validators.required),
      userpassword: new FormControl('', Validators.required),
    })
  }
  loginForm: FormGroup;
  get username() { return this.loginForm.get('username'); }
  get userpassword() { return this.loginForm.get('userpassword'); }

  iniciarSesion() {
    //console.log(this.loginForm.value);
    if (this.loginForm.invalid) {
      this.invalidUser = true;
    }
    this.user = this.loginForm.value;
    this.loginForm.reset();
    this.dataService.login(this.user).subscribe(
      (res: any) => {
        //console.log(res);
        localStorage.setItem('token', res.token);
        this.router.navigate(['']);
        this.dataService.isAuth();
      },
      err => {
        console.log(err);
        this.invalidUser = true;
      }
    )
    //this.invalidUser = true;
  }

  dismmisAlert(){
    this.invalidUser = false;
  }

}
