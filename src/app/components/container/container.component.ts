import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { from, Subject } from 'rxjs';
import { Sucursal } from 'src/app/models/Sucursal';
import { Region } from 'src/app/models/Region';
import { DataService } from '../../services/data.service';
import { DataTableDirective } from 'angular-datatables';
import { Lista } from 'src/app/Lista';
import * as XLSX from 'xlsx';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnDestroy, OnInit {

  createFormGroup(){
    return new FormGroup({
      fecha_1: new FormControl('', Validators.required),
      fecha_2: new FormControl('', Validators.required),
      regional: new FormControl('', Validators.required),
    });
  }
  regionalForm: FormGroup;
  get fecha_1() { return this.regionalForm.get('fecha_1'); }
  get fecha_2() { return this.regionalForm.get('fecha_2'); }
  get regional() { return this.regionalForm.get('regional'); }

  createFormGroupSucursal(){
    return new FormGroup({
      fechaSuc_1: new FormControl('', Validators.required),
      fechaSuc_2: new FormControl('', Validators.required),
      sucursalInput: new FormControl('', Validators.required),
    });
  }
  sucursalForm: FormGroup;
  get fechaSuc_1() { return this.sucursalForm.get('fechaSuc_1'); }
  get fechaSuc_2() { return this.sucursalForm.get('fechaSuc_2'); }
  get sucursalInput() { return this.sucursalForm.get('sucursalInput'); }

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  fileName= 'ExcelSheet.xlsx';
  lista: Lista[];
  selectSucursal = false;
  selectRegional = false;
  sucursal: Sucursal;
  region: Region;

  formSucursal() {
    //alert('Seleccionaste sucursal.')
    this.selectSucursal = true;
    this.selectRegional = false;
    this.lista = [];
    this.sucursalForm.reset();
    this.regionalForm.reset();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  formRegional() {
    this.selectRegional = true;
    this.selectSucursal = false;
    this.sucursalForm.reset();
    this.regionalForm.reset();
    this.lista = [];
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });

  }

  constructor(/*private http: HttpClient */private dataService: DataService) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      lengthMenu: [10, 25, 50, 200],
      language: {
        url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json'
      }
    };
    /*this.dataService.getData().subscribe(data => {
      //console.log(data);
      this.lista = data;
      console.log(this.lista);
    });*/
    this.dataService.getOperaciones().subscribe((res: any) => {
      //console.log(data);
      this.lista = res;
      this.dtTrigger.next();
      console.log(this.lista);
    });
    this.regionalForm = this.createFormGroup();
    this.sucursalForm = this.createFormGroupSucursal();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  findByDateSucursal(): void {
    if(this.sucursalForm.invalid) {
      alert("Completa campos");
    } else {
      //this.selectSucursal = false;
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
      this.sucursal = this.sucursalForm.value;
      this.dataService.getDesembolsoByDateSucursal(this.sucursal).subscribe((res: any) => {
        //console.log(this.sucursal);
        console.log(res);
        this.lista = res;
        this.dtTrigger.next();
      });
    /*this.dataService.getDesembolsoByDateSucursal(this.sucursal).subscribe( 
        res => console.log(res),
        err => console.log(err)
      );*/
    }
  }

  findByDateRegional(): void {
    if(this.regionalForm.invalid) {
      alert("Completa campos");
    } else {
      //alert('Campos completados.')
      //this.selectRegional = false;
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
      this.region = this.regionalForm.value;
      this.dataService.getDesembolsoByDateRegion(this.region).subscribe((res: any) => {
        console.log(this.region);
        //console.log(res);
        this.lista = res;
        this.dtTrigger.next();
      });
    }
  }

  exportexcel(): void {
    /* pass here the table id */
    let element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);

  }

}

