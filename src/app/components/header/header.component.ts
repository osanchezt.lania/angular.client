import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLogin: boolean;
  constructor(private dataService: DataService, private router: Router) { 
    if (!this.dataService.isAuth()){
      //this.isLogin = true;
    }
  }

  ngOnInit(): void {
    this.dataService.isLogged.subscribe((res) => (this.isLogin = res));
  }

  isLoggedout(){
    //this.isLogin = false;
    this.dataService.isLogout();
    this.router.navigate(['login']);
  }

}
