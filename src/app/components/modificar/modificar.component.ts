import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Lista } from 'src/app/Lista';
import { Sucursal } from 'src/app/models/Sucursal';
import { Region } from 'src/app/models/Region';
import { Desembolso } from '../../models/Desembolso';
import { DataService } from '../../services/data.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-modificar',
  templateUrl: './modificar.component.html',
  styleUrls: ['./modificar.component.css']
})
export class ModificarComponent implements OnInit, OnDestroy {

  lista: Lista[];
  sucursal: Sucursal;
  region: Region;
  desembolso: Desembolso = {
    id: null,
    nombre_ac: '',
    clientes: null,
    monto: null,
    banco: '',
    fecha_desembolso: '',
    sucursal: '',
    regional: ''
  };
  selectSucursal = false;
  selectRegional = false;
  seenButton = false;
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit(): void {
    this.regionalForm = this.createFormGroup();
    this.sucursalForm = this.createFormGroupSucursal();
    this.getOperaciones();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      lengthMenu: [10, 25, 50, 200],
      language: {
        url: '//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json'
      }
    };
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  getOperaciones () {
    this.dataService.getOperaciones().subscribe((res: any) => {
      //console.log(data);
      this.lista = res;
      this.dtTrigger.next();
      console.log(this.lista);
    });
  }

  formSucursal() {
    //alert('Seleccionaste sucursal.')
    this.selectSucursal = true;
    this.selectRegional = false;
    this.sucursalForm.reset();
    this.regionalForm.reset();
    this.lista = [];
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  findByDateSucursal(): void {
    if(this.sucursalForm.invalid) {
      alert("Completa campos");
    } else {
      //this.selectSucursal = false;
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
      this.sucursal = this.sucursalForm.value;
      this.dataService.getDesembolsoByDateSucursal(this.sucursal).subscribe( (res: any) => {
        //console.log(this.sucursal);
        console.log(res);
        this.lista = res;
        this.dtTrigger.next();
      });
    }
  }

  formRegional() {
    this.selectRegional = true;
    this.selectSucursal = false;
    this.sucursalForm.reset();
    this.regionalForm.reset();
    this.lista = [];
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  findByDateRegional(): void {
    if(this.regionalForm.invalid) {
      alert("Completa campos");
    } else {
      //alert('Campos completados.')
      //this.selectRegional = false;
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
      this.region = this.regionalForm.value;
      this.dataService.getDesembolsoByDateRegion(this.region).subscribe((res: any) => {
        console.log(this.region);
        //console.log(res);
        this.lista = res;
        this.dtTrigger.next();
      });
    }
  }

  //validacion de formulario REGIONAL
  createFormGroup(){
    return new FormGroup({
      fecha_1: new FormControl('', Validators.required),
      fecha_2: new FormControl('', Validators.required),
      regional: new FormControl('', Validators.required),
    });
  }
  regionalForm: FormGroup;
  get fecha_1() { return this.regionalForm.get('fecha_1'); }
  get fecha_2() { return this.regionalForm.get('fecha_2'); }
  get regional() { return this.regionalForm.get('regional'); }

  //validacion de formulario SUCURSAL
  createFormGroupSucursal(){
    return new FormGroup({
      fechaSuc_1: new FormControl('', Validators.required),
      fechaSuc_2: new FormControl('', Validators.required),
      sucursalInput: new FormControl('', Validators.required),
    });
  }
  sucursalForm: FormGroup;
  get fechaSuc_1() { return this.sucursalForm.get('fechaSuc_1'); }
  get fechaSuc_2() { return this.sucursalForm.get('fechaSuc_2'); }
  get sucursalInput() { return this.sucursalForm.get('sucursalInput'); }

  editDesembolso(item) {
    //opcion 1 para obtener desembolso por ID apuntando al back
    this.dataService.getOperacionesById(item.id).subscribe(
      res => {
        //console.log(res);
        //this.desembolso = res;
        this.seenButton = true;
        const fecha = item.fecha_desembolso;
        const resFecha = fecha.split("T");
        const fecha_formateada = resFecha[0];
        this.desembolso.id = item.id;
        this.desembolso.nombre_ac = item.nombre_ac;
        this.desembolso.clientes = item.clientes;
        this.desembolso.monto = item.monto;
        this.desembolso.banco = item.banco;
        this.desembolso.fecha_desembolso = fecha_formateada;
        this.desembolso.sucursal = item.sucursal;
        this.desembolso.regional = item.regional;
      },
      err => console.log(err)
    );
    //this.router.navigate(['/agregar']);


    //opcion 2 para obtener desembolso obteniendo los datos interpolados en la vista
    /*this.seenButton = true;
    const fecha = item.fecha_desembolso;
    const resFecha = fecha.split("T");
    const fecha_formateada = resFecha[0];
    //console.log(item);
    this.desembolso.id = item.id;
    this.desembolso.nombre_ac = item.nombre_ac;
    this.desembolso.clientes = item.clientes;
    this.desembolso.monto = item.monto;
    this.desembolso.banco = item.banco;
    this.desembolso.fecha_desembolso = fecha_formateada;
    this.desembolso.sucursal = item.sucursal;
    this.desembolso.regional = item.regional;*/
  }

  updateDesembolso(desembolso) {
    if (this.desembolso.nombre_ac == '' || this.desembolso.clientes == null
        || this.desembolso.monto == null
        || this.desembolso.banco == ''
        || this.desembolso.fecha_desembolso == ''
        || this.desembolso.sucursal == ''
        || this.desembolso.regional == '') {
      alert('Completa campos');
    } else {
      this.seenButton = false;
      //console.log(desembolso.id);
      //console.log(desembolso);
      this.dataService.updateDesembolso(desembolso.id, desembolso).subscribe(
        res => {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
          });
          console.log(res);
          this.getOperaciones();
          //this.dtTrigger.next();
        },
        err => console.log(err)
      );
      this.desembolso = {
        id: null,
        nombre_ac: '',
        clientes: null,
        monto: null,
        banco: '',
        fecha_desembolso: '',
        sucursal: '',
        regional: ''
      };
    }
  }

  clearForm(){
    this.seenButton = false;
    this.desembolso = {
      id: null,
      nombre_ac: '',
      clientes: null,
      monto: null,
      banco: '',
      fecha_desembolso: '',
      sucursal: '',
      regional: ''
    };
  }
  
}
