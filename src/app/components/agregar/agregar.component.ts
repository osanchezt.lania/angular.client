import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Desembolso } from '../../models/Desembolso';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {

  desembolso: Desembolso;

  constructor(private dataService: DataService, private router: Router) { }

  createFormGroup(){
    return new FormGroup({
      nombre_ac: new FormControl('', [Validators.required, Validators.minLength(5)]),
      clientes: new FormControl('', Validators.required),
      monto: new FormControl('', Validators.required),
      banco: new FormControl('', Validators.required),
      fecha_desembolso: new FormControl('', Validators.required),
      sucursal: new FormControl('', Validators.required),
      regional: new FormControl('', Validators.required),
    });
  }

  desembolsoForm: FormGroup;
  //lista: Lista [];
  /*lista: Lista = {
    nombre_ac: '',
    clientes: 0,
    monto: 0,
    banco: '',
    fecha_desembolso: '',
    sucursal: '',
    regional: '',
  };*/

  ngOnInit(): void {
    this.desembolsoForm = this.createFormGroup();
  }

  saveDesembolso() {
    if (this.desembolsoForm.valid) {
      this.desembolso = this.desembolsoForm.value;
      //console.log(this.desembolso);
      this.desembolsoForm.reset();
      this.dataService.saveDesembolso(this.desembolso).subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/'])
        },
        err => console.log(err)
      )
    } else {
      alert('Completa los campos del Formulario.')
    }
  }

  onResetForm() {
    this.desembolsoForm.reset();
  }

  get nombre_ac() { return this.desembolsoForm.get('nombre_ac'); }
  get clientes() { return this.desembolsoForm.get('clientes'); }
  get monto() { return this.desembolsoForm.get('monto'); }
  get banco() { return this.desembolsoForm.get('banco'); }
  get fecha_desembolso() { return this.desembolsoForm.get('fecha_desembolso'); }
  get sucursal() { return this.desembolsoForm.get('sucursal'); }
  get regional() { return this.desembolsoForm.get('regional'); }

}
