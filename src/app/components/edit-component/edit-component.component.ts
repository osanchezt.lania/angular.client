import { Component, OnInit } from '@angular/core';
import { Desembolso } from '../../models/Desembolso';
import { Lista } from 'src/app/Lista';
import { DataService } from '../../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-component',
  templateUrl: './edit-component.component.html',
  styleUrls: ['./edit-component.component.css']
})
export class EditComponentComponent implements OnInit {

  inputDisabled: boolean = true;
  seenButtonSave: boolean = false;
  seenButtonEdit: boolean = true;
  lista: Lista;
  desembolso: any = {
    id: null,
    nombre_ac: '',
    clientes: null,
    monto: null,
    idbanco: '',
    fecha_desembolso: '',
    idsucursal: '',
    idregional: '',
  };

  constructor(
    private dataService: DataService,
    private activedRoute : ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getDesembolso();
  }

  getDesembolso(){
    const params = this.activedRoute.snapshot.params;
    if(params.id) {
      this.dataService.getOperacionesById(params.id).subscribe(
        res => {
          this.lista = res;
          //console.log(this.lista);
        },
        err => console.error(err)
      );
    }
  }

  editDesembolso(item) {
    //console.log(item);
    this.seenButtonEdit = false;
    this.seenButtonSave = true;
    this.inputDisabled = false;
    const fecha = item.fecha_desembolso;
    const resFecha = fecha.split("T");
    const fecha_formateada = resFecha[0];
    item.fecha_desembolso = fecha_formateada;
    /*
      this.dataService.getOperacionesById(item.id).subscribe(
      res => {
        //console.log(res);
        //this.desembolso = res;
        this.seenButton = true;
        const fecha = item.fecha_desembolso;
        const resFecha = fecha.split("T");
        const fecha_formateada = resFecha[0];
        this.desembolso.id = item.id;
        this.desembolso.nombre_ac = item.nombre_ac;
        this.desembolso.clientes = item.clientes;
        this.desembolso.monto = item.monto;
        this.desembolso.banco = item.banco;
        this.desembolso.fecha_desembolso = fecha_formateada;
        this.desembolso.sucursal = item.sucursal;
        this.desembolso.regional = item.regional;
      },
      err => console.log(err)
    );
    */
  }

  updateDesembolso(item){
    this.desembolso.id = item.id;
    this.desembolso.nombre_ac = item.nombre_ac;
    this.desembolso.clientes = item.clientes;
    this.desembolso.monto = item.monto;
    this.desembolso.idbanco = item.idbanco;
    this.desembolso.fecha_desembolso = item.fecha_desembolso;
    this.desembolso.idsucursal = item.idsucursal;
    this.desembolso.idregional = item.idregional;
    //console.log(this.desembolso);
    if (this.desembolso.nombre_ac == '' || this.desembolso.clientes == ''
        || this.desembolso.monto == ''
        || this.desembolso.idbanco == ''
        || this.desembolso.fecha_desembolso == ''
        || this.desembolso.idsucursal == ''
        || this.desembolso.idregional == '') {
      alert('Completa campos');
    } else {
      this.dataService.updateDesembolso(this.desembolso.id, this.desembolso).subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/modificar'])
        },
        err => console.log(err)
      );
    }
  }

}
