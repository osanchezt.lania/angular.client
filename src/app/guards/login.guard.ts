import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { DataService } from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor (private dataService: DataService, private router: Router) { }
  
  canActivate(): boolean{
    if (!this.dataService.isLogin()){
      console.log('Usuario con sesión iniciada');
      this.router.navigate(['']);
      return false;
    }
    return true;
  }
}
