import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DataService } from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor (private dataService: DataService, private router: Router) { }

  canActivate(): boolean {

    if (!this.dataService.isAuth()){
      console.log('Token no válido o token expiró');
      this.router.navigate(['login']);
      return false;
    }

    return true;
  }
  
}
