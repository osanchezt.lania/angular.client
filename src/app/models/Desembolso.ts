export interface Desembolso {
    "id"?: number;
    "nombre_ac": string;
    "clientes": number;
    "monto": number;
    "banco": string;
    "fecha_desembolso": string;
    "sucursal": string;
    "regional": string;
}